import bitbucketjs from '../src/bitbucket';

export const config = {
  apiRoot: process.env.TEST_BB_API_ROOT || 'https://api.bitbucket.org',
  username: process.env.TEST_BB_USERNAME,
  password: process.env.TEST_BB_PASSWORD
}

export function getClient() {
  return bitbucketjs(config);
}

process.on('unhandledRejection', function(reason, p){
  console.error(reason.stack);
});
