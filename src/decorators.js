import _get from 'lodash.get'

export function authenticated(request) {
  return function decorator(target, key, descriptor) {
    const fn = descriptor.value;
    return {
      ...descriptor,
      value: function _authenticated() {
        if (request._bb_authenticated) {
          return fn.apply(this, arguments);
        } else {
          return request._Promise.reject(new Error(`method "${key}" requires authentication.`))
        }
      }
    }
  }
}
